#! /usr/bin/env python
# -*- encoding: UTF-8 -*-

"""Example: Use setAngles Method"""

import qi
import argparse
import sys
import time
import almath

class WatchManager() :

    def watch (self, angle, session) :
        motion_service  = session.service("ALMotion")
        motion_service.setStiffnesses("Head", 1)
        names            = "HeadYaw"
        angles           = angle * almath.TO_RAD
        fractionMaxSpeed = 0.2
        motion_service.setAngles(names,angles,fractionMaxSpeed)
        time.sleep(3.0)

class MoveManager() :

    def move (self, dist, session) :
        #animation = session.service('ALAnimationPlayer')
        motion = session.service('ALMotion')
        motion.wakeUp()
        motion.moveInit()
        motion.moveTo(dist, 0, 0, _async=True)
        #animation.runTag('hello', _async=True)







