#! /usr/bin/env python
# -*- encoding: UTF-8 -*-

"""Example: Use setAngles Method"""

import qi
import argparse
import sys
import time
import almath
import behavior as bh
import detect as dt

def main(session):

    try :
        tts = session.service("ALTextToSpeech")
        life_service = session.service("ALAutonomousLife")
        animation = session.service('ALAnimationPlayer')
        animated_speech = session.service("ALAnimatedSpeech")
        motion_service = session.service("ALMotion")
        posture_service = session.service("ALRobotPosture")
        sound_detect_service = session.service("ALSoundDetection")
        audio = session.service("ALAudioDevice")

        # to stop "hi"
        # audio = session.service("ALListeningMovement")
        # audio.setEnabled(False)
        #
        # audio = session.service("ALAudioDevice")
        # audio.muteAudioOut(False)

        audio = session.service("ALSoundDetection")
        audio.setParameter("Sensitivity", 0.0)
        #audio.unsubscribe('SoundDetected')

        """
        Body animations:
        1. 'animations/Stand/Gestures/BowShort_1'  --->   Greeting at the beginning
        2. 'animations/Stand/Gestures/Enthusiastic_4' , 'animations/Stand/Emotions/Positive/Peaceful_1'  ---> talking
    
        ex)
        animated_speech.say("Hi ^start(animations/Stand/BodyTalk/BodyTalk_12), welcome to the HCI lab!")
    
        """
        # distance update
        gaze = dt.GazeAnalysis(app)

        sound_detect_service.setParameter("Sensitivity", 0.0)
        # sound_detect_service.unsubscribe()
        life_service.setAutonomousAbilityEnabled("BasicAwareness", False)
        life_service.setAutonomousAbilityEnabled("AutonomousBlinking", True)
        life_service.setAutonomousAbilityEnabled("BackgroundMovement", True)
        life_service.setAutonomousAbilityEnabled("ListeningMovement", True)
        life_service.setAutonomousAbilityEnabled("SpeakingMovement", True)
        tts.setParameter("speed", 100)

        # custom classes
        watch_manager = bh.WatchManager()
        move_manager = bh.MoveManager()
        speech_analyzer = dt.SpeechAnalysis()



        '''
        #########################################
        ############### GREETINGS ###############
        #########################################
        '''
    
        # life_service.setAutonomousAbilityEnabled("BasicAwareness", True)
        # if gazing detected
        posture_service.goToPosture("StandInit", 0.6)
        time.sleep(2)
        gaze = dt.GazeAnalysis(app)
        if (gaze.is_detected()):
            life_service.setAutonomousAbilityEnabled("BasicAwareness", False)
            animation.run('animations/Stand/Gestures/BowShort_1')
            # motion_service.wakeUp()
            # watch_manager.watch(0, session)
            animated_speech.say(
                "^start(animations/Stand/Emotions/Positive/Peaceful_12) welcome to harry's shoes store")  # move code need to be added ***
            animated_speech.say(
                "First, you can read the catalog and there is information about products")
            posture_service.goToPosture("StandInit", 0.4)
    
        #time.sleep(1)







        '''
        ##########################################
        ###############  APPROACH  ###############
        ##########################################
        '''

        # puts its eys toward to customer from time to time
        # life_service.setAutonomousAbilityEnabled("BasicAwareness", True)
        print "moving head"
        angle = [-30, 30, -15, 0]
        for i in range(0, len(angle)):
            watch_manager.watch(angle[i], session)
            time.sleep(1)
    
        distance = gaze.distance
        if (distance is not None):
            print("distance : " + str(distance))
            move_manager.move(distance * 0.2, motion_service)
        else:
            print("distance : default value")
            move_manager.move(0.2, motion_service)
    
        while (True):
            # the robot come next to customer
            # Have you decided what to order, I can help you to make better decision
            life_service.setAutonomousAbilityEnabled("BackgroundMovement", True)
            watch_manager.watch(0, session)
            animated_speech.say("^start(animations/Stand/Gestures/BodyTalk_12)"
                                "Do you need help to make decision?"
                                "^wait(animations/Stand/Gestures/BodyTalk_12)")
    
            intent_result = speech_analyzer.intent_classify(session, ['yes', 'sure','okay','yeah'],
                                                            ['no', 'not', "don't", "It's okay"])
    
            if intent_result == 1:
                print("positive")
                distance = gaze.distance
                if (distance is not None):
                    print("distance : " + str(distance))
                    move_manager.move(distance * 0.3, motion_service)
                else:
                    print("distance : default value")
                    move_manager.move(0.3, motion_service)
    
                break
    
            elif intent_result == 2:
                print("negative")
                tts.setParameter("speed", 95)
                animated_speech.say(
                    "^start(animations/Stand/Emotions/Positive/Peaceful_12) "
                    "Then, Take your time and look around."
                    "^wait(animations/Stand/Emotions/Positive/Peaceful_12) ")
    
    
            time.sleep(10)
        posture_service.goToPosture("Stand", 0.6)





        '''
        ##########################################
        ############  RECOMMENDATIONS  ###########
        ##########################################
        '''
        life_service.setAutonomousAbilityEnabled("BackgroundMovement", False)
        cate = ['slippers', 'sandals', 'boots']
        cate_dict = {'slippers':['oxfolds','heel'], #sleeper, slippers, sleepers
                     'sandals':['boat shoes','loafers'],
                     'boots':['flat','ankle'] } #put, puts, butch, Butch



        break_flag = 0
        stop = 0

        user_preference = []
        while True:

            # CATEGORY
            tts.setParameter("speed", 90)
            animated_speech.say("^start(animations/Stand/Gestures/Enthusiastic_4)"
                                
                                "What category of shoes do you want? "
                                "^wait(animations/Stand/Gestures/Enthusiastic_4)")

            animated_speech.say("There is" + str(len(cate)) + "categories of shoes")
            for i in cate :
                animated_speech.say(i)

            speech_analyzer = dt.SpeechAnalysis()
            status, picked_cate = speech_analyzer.catching(session, cate)

            print(status, picked_cate)
            if status == 0 : #stop
                stop = 1
                break
            elif status == 2 : #picking non-existing category
                tts.say('Can you choose proper category, again?')

            else : #picking proper cateory / status == 1
                animated_speech.say(
                    "^start(animations/Stand/Emotions/Positive/Peaceful_12) "
                    'Ok, you choose' + picked_cate[0])
                #tts.say('Ok, you choose' + picked_cate[0])
                user_preference.append(picked_cate[0])

                # 1-1
                while True:
                    sub_cate = cate_dict.get(picked_cate[0])
                    # SUB-CATEGORY
                    animated_speech.say("^start(animations/Stand/Gestures/Enthusiastic_4)"
                                        "There is" + str(len(sub_cate)) + "sub categories"
                                        "^wait(animations/Stand/Gestures/Enthusiastic_4)")

                    for i in sub_cate: animated_speech.say(i)

                    tts.say("which kind of sub category do you want?")
                    status, picked_subcate = speech_analyzer.catching(session, sub_cate)

                    if status == 0:  # stop
                        break_flag = 1
                        stop = 1
                        break
                    elif status == 2:  # picking non-existing category
                        tts.say('Can you choose proper category, again?')

                    else:  # picking proper cateory / status == 1
                        tts.say('Ok, you choose' + picked_subcate[0])
                        user_preference.append(picked_subcate[0])
                        break_flag = 1
                        break

                # 1-2
                if break_flag == 1 :
                    break

        if stop != 1 :
            # GETTING DATA FROM DB BASED ON PREFERENCE
            tts.setParameter("speed", 85)
            animated_speech.say("^start(animations/Stand/Gestures/BodyTalk_12)"
                                "based on your preference")
            # time.sleep(1)
            # animated_speech.say("^start(animations/Stand/Gestures/BodyTalk_12)"
            #                     'and '.join(map(str, user_preference)))
            animated_speech.say("^start(animations/Stand/Gestures/Enthusiastic_4)"
                                "I suggest product A and B ")
            animated_speech.say("^start(animations/Stand/Gestures/Enthusiastic_4)"
                                "What do you think about t"
                                "these 2 shoes ? "
                                "Left one is a more spotty style ")
            tts.setParameter("speed", 80)
            tts.say("Right one is made of leather.")
            posture_service.goToPosture("Stand", 0.6)






        
        '''
        ##########################################
        ##############  CONFIRMATION  ############
        ##########################################
        '''
        ## test blocks ##
        # move_manager.move(0.2,session)
        distance = gaze.distance
        if (distance is not None):
            print("distance : " + str(distance))
            move_manager.move(-distance * 0.4, motion_service)
        else:
            print("distance : default value")
            move_manager.move(-0.2, motion_service)
        

    except :
        print('error')
        posture_service.goToPosture("Stand", 0.4)


if __name__ == "__main__":
    session = qi.Session()
    PepperIP = "192.168.1.103"
    try:

        connection_url = "tcp://" + PepperIP + ":" + "9559"
        session.connect(connection_url)
        app = qi.Application(["GazeAnalysis", "--qi-url=" + connection_url])

    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + PepperIP + "\" on port " + "9559" + ".\n"
                                                                                       "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)

    main(session)

