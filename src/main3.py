#! /usr/bin/env python
# -*- encoding: UTF-8 -*-

"""Example: Use setAngles Method"""

import qi
import argparse
import sys
import time
import almath
import behavior as bh
import detect as dt

def main(session):
    tts = session.service("ALTextToSpeech")
    life_service = session.service("ALAutonomousLife")
    animation = session.service('ALAnimationPlayer')
    animated_speech = session.service("ALAnimatedSpeech")
    motion_service = session.service("ALMotion")

    # custom classes
    watch_manager = bh.WatchManager()
    move_manager = bh.MoveManager()

    ## 0.greetings ##

    # if gazing detected
    watch_manager.watch(0, session)
    gaze = dt.GazeAnalysis(app)
    # if(gaze.is_detected()):
    # time.sleep(2)
    animation.run('animations/Stand/Gestures/BowShort_1')
    # motion_service.wakeUp()
    # watch_manager.watch(0, session)
    animated_speech.say(
        "^start(animations/Stand/Emotions/Positive/Peaceful_12) welcome to yun's dishes")  # move code need to be added ***
    pass
    # time.sleep(2)
    animated_speech.say(
        "please, read the menu and decide which one to order")
    time.sleep(1)
    animated_speech.say(
        "if you ^start(animations/Stand/BodyTalk/BodyTalk_12), have any question, you can ask me at any time")
    # tts.say("if you have any question, you can ask me at any time")
    time.sleep(1)
    pass

    ## 1.approach ##

    # puts its eys toward to customer from time to time
    # angle = [-30,60,0]
    # for i in range(0,len(angle)) :
    #     print "moving",i,"/3"
    #     watch_manager.watch(angle[i],session)
    #     time.sleep(2) if angle == 0 else time.sleep(2)

    # When it hears the customer ask, keywords like "hi", "pepper", "help"
    speech_analyzer = dt.SpeechAnalysis()
    catch_words = ['hi','Pepper','help', 'pepper','hello']
    if (speech_analyzer.catch(catch_words)) :


        # it goes to the customer (Slowly).
        pass
        # #temp reaction
        # watch_manager.watch(0, session)
        # tts.say("Yes!! sir")
        # animation_player_service = session.service("ALAnimationPlayer")
        # animation_player_service.run("animations/Stand/Gestures/Hey_1")

        #motion.wakeUp()
        #motion.moveInit()
        #motion.moveTo(0.2, 0, 0, _async=True)
        animated_speech.say(

            "^start(animations/Stand/BodyTalk/BodyTalk_12)yes. sure")
        # tts.say("if you have any question, you can ask me at any time")
    else :
        return


    # say something like Yes. Have you decided what to order, I can help you to make better decision.
    watch_manager.watch(0, session)
    tts.say("Have you decided what to order?, I can help you to make better decision")


    ## test blocks ##

    #menu
    menus = speech_analyzer.ordered_menu(session)
    tts.say("Okay, then you ordered !~")

    for i in range(0,len(menus)) :
        if i == len(menus)-1 : tts.say("and~")
        tts.say(menus[i])

    tts.say("I will be back soon")



if __name__ == "__main__":
    session = qi.Session()
    PepperIP = "192.168.1.103"
    try:

        connection_url = "tcp://" + PepperIP + ":" + "9559"
        session.connect(connection_url)
        app = qi.Application(["GazeAnalysis", "--qi-url=" + connection_url])

    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + PepperIP + "\" on port " + "9559" +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)

    main(session)
    
