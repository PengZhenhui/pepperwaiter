#! /usr/bin/env python
# -*- encoding: UTF-8 -*-

import time

class SpeechAnalysis():
    pass


class GazeAnalysis(object):
    def __init__(self, app):
        super(GazeAnalysis, self).__init__()
        app.start()
        session = app.session
        self.memory = session.service("ALMemory")

        self.subscriber1 = self.memory.subscriber("GazeAnalysis/PersonStartsLookingAtRobot")
        self.subscriber1.signal.connect(self.get_id)

        self.subscriber2 = self.memory.subscriber("GazeAnalysis/PersonStopsLookingAtRobot")
        self.subscriber2.signal.connect(self.delete_id)
        self.gaze = session.service("ALGazeAnalysis")
        self.gaze.subscribe("GazeAnalysis")
        self.got_gaze = False
        self.people_id = 0

    def get_id(self, id):  # raise when detect the human gaze
        if id == []:
            self.people_id = 0
        else:
            self.people_id = id

    def delete_id(self, id):  # raise when the cannot detect human gaze
        self.people_id = 0

    def on_gaze_tracked(
            self):  # print the head_angle, gaze_direction and looking_robot_score, detail in http://doc.aldebaran.com/2-5/naoqi/peopleperception/algazeanalysis-api.html?highlight=gaze#PeoplePerception/Person/%3CID%3E/IsLookingAtRobot
        if self.people_id == 0:
            print "Can not find a people"
        else:
            head_key = 'PeoplePerception/Person/' + str(self.people_id) + '/HeadAngles'
            gaze_direction_key = 'PeoplePerception/Person/' + str(self.people_id) + '/GazeDirection'
            is_looking_at_robot_key = 'PeoplePerception/Person/' + str(self.people_id) + '/LookingAtRobotScore'
            head_angle = self.memory.getData(head_key)
            gaze_direction = self.memory.getData(gaze_direction_key)
            looking_robot_score = self.memory.getData(is_looking_at_robot_key)
            print "I have catched your attention!"
            print(head_angle)
            print(gaze_direction)
            print(looking_robot_score)
            return (True)  #

    def is_detected(self):
        print "Start GazeAnalysis"
        try:
            while True:
                time.sleep(1)  #
                if (self.on_gaze_tracked()):  #
                    return (True)



        except KeyboardInterrupt:
            print "Interrupted by user, stopping GazeAnalysis"
            self.gaze.unsubscribe("GazeAnalysis")
            sys.exit(0)


def main(session):
    """
    This example uses the setAngles method and setStiffnesses method
    in order to control joints.
    """
    '''
    # Get the service ALMotion.
    watch_manager = WatchManager() 
    watch_manager.watch(-60)
    #watch_manager.watch(60)
    watch_manager.watch(0)
    

    # move
    motion = session.service('ALMotion')
    motion.wakeUp()
    motion.moveInit()
    motion.moveToward(0.2, 0.0, 0.0, _async=True)
    time.sleep(5)

    # tts 
    tts = session.service("ALTextToSpeech")
    tts.say("Anything you need?")
    '''
    animation = session.service('ALAnimationPlayer')
    motion = session.service('ALMotion')
    tts = session.service("ALTextToSpeech")
    tts.say("you come here")
    print('check1')
    gaze = gz.GazeAnalysis(app)
    if(gaze.run()):
        time.sleep(2)



        tts.say("welcome to yun's dishes") #welcome message
        #animation.run("animations/[posture]/Gestures/Hey_3") # need to add welcome gesture

        # need to move close
        # motion.wakeUp()
        # motion.moveInit()
        # motion.moveTo(0.2, 0, 0, _async=True)
        tts.say("please, read the menu and decide which one to order")


        tts.say("if you have any question, you can ask me at any time")
        # need to stay away
        # motion.moveInit()
        # motion.moveTo(-0.2, 0, 0, _async=True)





if __name__ == "__main__":
    session = qi.Session()
    PepperIP = "192.168.1.103"
    try:

        connection_url = "tcp://" + PepperIP + ":" + "9559"
        session.connect(connection_url)
        app = qi.Application(["GazeAnalysis", "--qi-url=" + connection_url])

    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + PepperIP + "\" on port " + "9559" +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)

    main(session)
    
