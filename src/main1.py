#! /usr/bin/env python
# -*- encoding: UTF-8 -*-

"""Example: Use setAngles Method"""

import qi
import argparse
import sys
import time
import almath
import behavior as bh
import detect as dt
import queryData as db
import random

def main(session):

    tts = session.service("ALTextToSpeech")
    life_service = session.service("ALAutonomousLife")
    animation = session.service('ALAnimationPlayer')
    animated_speech = session.service("ALAnimatedSpeech")
    motion_service = session.service("ALMotion")
    posture_service = session.service("ALRobotPosture")
    sound_detect_service = session.service("ALSoundDetection")


    # to stop "hi"
    # audio = session.service("ALListeningMovement")
    # audio.setEnabled(False)
    #
    # audio = session.service("ALAudioDevice")
    # audio.muteAudioOut(False)

    audio = session.service("ALSoundDetection")
    audio.setParameter("Sensitivity", 0.0)
    #audio.unsubscribe('SoundDetected')


    """
    Body animations:
    1. 'animations/Stand/Gestures/BowShort_1'  --->   Greeting at the beginning
    2. 'animations/Stand/Gestures/Enthusiastic_4' , 'animations/Stand/Emotions/Positive/Peaceful_1'  ---> talking
    """
    # distance update
    gaze = dt.GazeAnalysis(app)

    sound_detect_service.setParameter("Sensitivity", 0.0)
    life_service.setAutonomousAbilityEnabled("BasicAwareness", True)
    life_service.setAutonomousAbilityEnabled("AutonomousBlinking", True)
    life_service.setAutonomousAbilityEnabled("BackgroundMovement", True)
    life_service.setAutonomousAbilityEnabled("ListeningMovement", True)
    life_service.setAutonomousAbilityEnabled("SpeakingMovement", True)
    tts.setParameter("speed", 100)

    # custom classes
    watch_manager = bh.WatchManager()
    move_manager = bh.MoveManager()
    speech_analyzer = dt.SpeechAnalysis()
    db_manager = db.shoedb()




    '''
    #########################################
    ############### GREETINGS ###############
    #########################################
    '''

    # if gazing detected
    posture_service.goToPosture("StandInit", 0.6)
    time.sleep(2)

    if(gaze.is_detected()):
        life_service.setAutonomousAbilityEnabled("BasicAwareness", False)
        animation.run('animations/Stand/Gestures/BowShort_1')
        animated_speech.say(
            "^start(animations/Stand/Emotions/Positive/Peaceful_12) welcome to harry's shoes store")
        animated_speech.say(
            "First, you can read the catalog and there is information about products")
        posture_service.goToPosture("StandInit", 0.6)




    '''
    ##########################################
    ###############  APPROACH  ###############
    ##########################################
    '''

    # puts its eys toward to customer from time to time
    #life_service.setAutonomousAbilityEnabled("BasicAwareness", True)
    print "approach : moving head"
    angle = [-30,30, -15, 0]
    for i in range(0,len(angle)) :
        watch_manager.watch(angle[i],session)
        time.sleep(1)


    # the robot come next to customer
    distance = gaze.distance
    if (distance is not None):
        print("distance : " + str(distance))
        move_manager.move(distance * 0.7, motion_service)
    else:
        print("distance : default value")
        move_manager.move(0.7, motion_service)



    # Have you decided what to order, I can help you to make better decision
    #life_service.setAutonomousAbilityEnabled("BackgroundMovement", True)
    watch_manager.watch(0, session)
    animated_speech.say("^start(animations/Stand/Gestures/BodyTalk_12)"
                        "Let me give you a recommendation ! ")

    posture_service.goToPosture("Stand", 0.7)




    '''
    ##########################################
    ############  RECOMMENDATIONS  ###########
    ##########################################
    '''
    data = {'category':['Boots','Sandals','Slippers'],
            'gender':['Men','Women'],
            'toestyle':['Round Toe','Capped Toe','Round Toe']}


    flag = 0
    while True :

        query_dict = {}  # query_dict : {'category': 'Boots', 'gender': 'Men'}
        for key in data.keys :
            if random.randrange(1,11) < 7 :
                i = random.randrange(1, 4) - 1
                query_dict[key] = data.get(key)[i] #add key - value to query_dic
        if len(query_dict) :
            pass





        db_manager.queryShow(query_dict)

        # if flag == 0 :
        #     animated_speech.say("^start(animations/Stand/Gestures/Enthusiastic_4)"
        #                         "What do you think about t"
        #                         "these 2 shoes ? "
        #                         "Left one is a more spotty style ")
        #     tts.setParameter("speed", 80)
        #     tts.say("Right one is made of leather.")
        #
        #     life_service.setAutonomousAbilityEnabled("BackgroundMovement", True)
        #     #posture_service.goToPosture("Stand", 1.0)
        #     #life_service.setAutonomousAbilityEnabled("BasicAwareness", True)
        #     #life_service.setAutonomousAbilityEnabled("BackgroundMovement", True)
        #     # life_service.setAutonomousAbilityEnabled("BackgroundMovement", True)
        # else:
        #     animated_speech.say("^start(animations/Stand/Gestures/Enthusiastic_4)"
        #                         "What do you think about t"
        #                         "these 2 shoes ? "
        #                         "Left one is appropriate for young age girl ")
        #     tts.setParameter("speed", 100)
        #     tts.say("Right one is on sale")
        #
        #
        # time.sleep(2)
        # tts.setParameter("speed", 115)
        # animated_speech.say(
        #     "^start(animations/Stand/Emotions/Positive/BodyTalk_12) "
        #     "Do you like my recommendation ? ")
        # life_service.setAutonomousAbilityEnabled("BackgroundMovement", True)

        intent_result = speech_analyzer.intent_classify(session, ['yes','sure','like','OK','Okay'],
                                                        ['no','not',"don't","another"])
        if intent_result==1 :
            print( "positive" )
            tts.say("Thank you!")
            break

        elif intent_result==2 :
            print( "negative")
            tts.setParameter("speed", 95)
            animated_speech.say(
            "^start(animations/Stand/Emotions/Positive/Peaceful_12) "
            "Then, I will give you another suggestion")
            flag = 1
            time.sleep(2)






        '''
        ##########################################
        #############  CONFIRMATION  #############
        ##########################################
        '''


    ## test blocks ##
    # move_manager.move(0.2,session)
    distance = gaze.distance
    if (distance is not None):
        print("distance : " + str(distance))
        move_manager.move(-distance * 0.4, motion_service)
    else:
        print("distance : default value")
        move_manager.move(-0.2, motion_service)
        

        




if __name__ == "__main__":
    session = qi.Session()
    PepperIP = "192.168.1.103"
    try:

        connection_url = "tcp://" + PepperIP + ":" + "9559"
        session.connect(connection_url)
        app = qi.Application(["GazeAnalysis", "--qi-url=" + connection_url])

    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + PepperIP + "\" on port " + "9559" +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)

    main(session)
    
