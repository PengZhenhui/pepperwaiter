#! /usr/bin/env python
# -*- encoding: UTF-8 -*-
import numpy as np
import random
import pymongo
import json
import csv
import time
from scipy.io import loadmat
client = pymongo.MongoClient()
db = client.tasks

men_task = db.men_task
women_task = db.women_task
teen_task = db.teen_task


class Task_Manager:
    def __init__(self):
        self.flag = True

    def creatDB(self):
        data1_path = '../MenOxfords.csv'
        data2_path = '../WomenHeels.csv'
        data3_path = '../Sneakers.csv'
        men_task = db.men_task
        women_task = db.women_task
        teen_task = db.teen_task
        # The database and collection code.
        men_task.drop()
        women_task.drop()
        teen_task.drop()
        men_task = db.men_task
        women_task = db.women_task
        teen_task = db.teen_task


        with open(data1_path, 'rb') as f:
            spamreader1 = csv.reader(f, quotechar='|')
            for row in spamreader1:
                men_task_json = {
                    'index': int(row[0]),
                    'attribute_1': int(row[1]),
                    'attribute_2': int(row[2]),
                    'description': row[3],
                    'popularity': int(row[4])
                }
                #print(men_task_json)
                men_task.insert(men_task_json)
        print('Men Oxfords data has been store in client.tasks.men_task  !!!!!!')
        print(men_task.find().count())


        with open(data2_path, 'rb') as f:
            spamreader2 = csv.reader(f, quotechar='|')
            for row in spamreader2:
                women_task_json = {
                    'index': int(row[0]),
                    'attribute_1': int(row[1]),
                    'attribute_2': int(row[2]),
                    'description': row[3],
                    'popularity': int(row[4])
                }
                #print(men_task_json)
                women_task.insert(women_task_json)
        print('Women heels data has been store in client.tasks.women_task  !!!!!!')
        print(women_task.find().count())

        with open(data3_path, 'rb') as f:
            spamreader3 = csv.reader(f, quotechar='|')
            for row in spamreader3:
                teen_task_json = {
                    'index': int(row[0]),
                    'attribute_1': int(row[1]),
                    'attribute_2': int(row[2]),
                    'description': row[3],
                    'popularity': int(row[4])
                }
                #print(men_task_json)
                teen_task.insert(teen_task_json)
        print('Teen sneakers data has been store in client.tasks.teen_task  !!!!!!')
        print(teen_task.find().count())



    def generate_recommendation(self, preferences, collection, previous_recommend):
        matches = collection.find(preferences)
        matches_list = []
        for match in matches:
            matches_list.append(match)
        popularity_list = sorted(matches_list, key=lambda x:x['popularity'], reverse = True)
        count = 0
        while True:
            if popularity_list[count]['index'] in previous_recommend:
                count += 1
                continue
            if count == len(popularity_list):
                self.flag = False
            print(count)
            break
        if self.flag:
            return popularity_list[count]
        else:
            self.flag = True
            print('Can not find match shoes that have not been recommended before.')
            return -1 


if __name__ == '__main__':
    tasks_manager = Task_Manager()
    previous_recommend = []
    tasks_manager.creatDB()
    men_attribute_1 = ['black', 'brown']
    men_attribute_2 = ['dress', 'casual']

    women_attribute_1 = ['black', 'beige']
    women_attribute_2 = ['dress', 'casual']

    teen_attribute_1 = ['black', 'white']
    teen_attribute_2 = ['running', 'skate']


    while True:
        attribute_1_prefer = raw_input('black or brown: ')
        if attribute_1_prefer == 'black':
            attribute_1_prefer = 1
        else:
            attribute_1_prefer = 2
        attribute_2_prefer = raw_input('dress or casual: ')
        if attribute_2_prefer == 'dress':
            attribute_2_prefer = 1
        else:
            attribute_2_prefer = 2
        preferences = {'attribute_1': attribute_1_prefer, 'attribute_2': attribute_2_prefer}
        #print(preferences)
        recommendation = tasks_manager.generate_recommendation(preferences, men_task, previous_recommend)
        recommend_index = recommendation['index']
        recommend_good_words = recommendation['description']
        print('The index of recommended shoes is {}.'.format(recommend_index))
        print('Robot: ' + recommend_good_words)
        previous_recommend.append(recommend_index)


