# Pepper Waiter

## 1. Goal
The project is developed by HKUST HCI Initiative on the purpose of evaluate the effects of proactive, adaptive and reactive robot strategies on supporting customers decision-making. We implement three kind of strategies in each step of the supporting decision-making flow: 

  - Approach
  - Preference elicitation and recommendation
  - Justification
  - Revision

## 2. Programming environment

### 2.1 Pepper robot

- naoqi 2.5.10 API
- Wifi: PepperRobot  (password: hcihkust)
- Pepper IP: 192.168.1.103

### 2.2 Google speech recognition

- Install google cloud API
- Create google project and download the credentials 

### 2.3 Others

- TBD

## 3. Tasks and requirement for the robot

We lead the customer to the experiment room, and we leave. The robot's task begin.

- Greet the customer (`Welcome gesture and words` and `self-introduction`)
- Illustrate the task (`Gesture and words like please read the menu and decide which one to order`)
- Leave (`Say something like "if you have any question, you can ask me at any time", then stay away from the customer`)

### 3.1 Approach

#### 3.1.1 Proactive approach

`The robot puts its eys toward to customer from time to time, e.g. every 5s.`

- After a while (`a default time from pilot study or Wizard-of-Oz`), the robot come next to customer (`Slowly`). 
- And say something like `Have you decided what to order, I can help you to make better decision.` 

#### 3.1.2 Adaptive approach

`The robot puts its eys toward to customer from time to time, e.g. every 5s.`

- After a while (`a default time from pilot study or Wizard-of-Oz`), the robot `keep its eyes on the customer`, and `show some proper gestures to catch customers attention`. 
- If the customer looks at the robot for 1s (`human gaze detection`), we say that the robot has catched customer's attention. 
- The robot comes a little bit closer to the customer (`move close a little bit, but still have large distance`), and say something like `Hi, do you need my help to order food?`


#### 3.1.3 Reactive approach

`The robot puts its eys toward to customer from time to time, e.g. every 5s.`

- When it hears the customer ask it (`speech recognition, keywords like "hi", "pepper", "help"`), it goes to the customer (`Slowly`).
- After coming next to the customer, say something like `Yes. Have you decided what to order, I can help you to make better decision.`

## 3.2 Preference elicitation and recommendation

### 3.2.1 Proactive

After approaching and greeting the customers, the robot proactively recommend some specials to the customers, like `Let me recommend some speciials to you, for xxx, we have xxx,xxx,xxx; for xxx, we have xxx,xxx,xxx...` After recommendation, stop for a certain time (e.g. 2s), and then jump into proactive justification step. 

### 3.2.2 Adaptive

After approaching and greeting the customers, the robot elicit the preference of customers step by step. There are several potential settings. For example, 

- `Want to eat spicy food?` based on the result, we further filter the potential dsihes, e.g. `Do you want to try some fishes?`. Until we narrow down to serveral choices (e.g. 3), then the robot recommend these choices `We recommend you xxx,xxx,xxx...` After recommendation, stop for a certain time (e.g. 2s), and then jump into adaptive justification step. 
- If the user task is to choose 3 dishes in different categories (but can only choose one in one category), and we have 6 categories (each category has the same number of dishes), then the robot can start from the 1st category `Do you want to order some foods in category 1?` to the last category if the customer still pick 3 food. This is an iterative step. 
- Others, need to discuss.

### 3.2.3 Reactive

After approaching and greeting the customers, the robot elicit the preference of customers by asking open questions. There are also some different settings based on the user study design. For example,

- `What kind of dishes do you want to order?` based on the results, we recommend some choices. After recommendation, stop for a certain time (e.g. 2s), and then jump into reactive justification step.  
- `Which category of dishes do you want to order?` based on the results, we recommend some choices. After recommendation, stop for a certain time (e.g. 2s), and then jump into reactive justification step.  

## 3.3 Justification

### 3.3.1 Proactive

After recommending some choices, the robot `explain all aspect of the dishes to the customers` until the customer stops it. Then say something like `please tell your choice if you have made up your mind.` Go into next step. 

### 3.3.2 Adaptive

After recommending some choices, the robot `explain one or two aspect of the dishes to the customers` and ask `do you need more information about the dishes?`. If yes, explain based on customers response. If no, then say something like `please tell your choice if you have made up your mind.` Go to next step.

### 3.3.3 Reactive

After recommending some choices, the robot wait until the customer ask questions, or say something to let customer ask question if he/she has question. Then based on the people's questions, the customer explain the recommended dishes if needed. Go to next step.

## 3.4 Follow-up (Checking)

### 3.4.1 Proactive

After justification, the robot will `wait for a certain time (e.g. 4s)`, and then `strongly recommend one dish` or `recommend other dishes that are out of previous recommendation`.

### 3.4.2 Adaptive

After justification, the robot will `wait for a certain time (e.g. 4s)` and then `ask if he had decided` or `ask if he has any questions or comments about the recommended choices`. 

### 3.4.3 Reactive

After justification, the robot will keep waiting until the customer asks it questions or gives comments on the recommended choices or makes decisions

# 4. Modules

## 4.1 Speech stream recognition

Understand what the customer say and request. The output can be used for several purposes:

- The final result, is used to search the dishes database and generate the responses or trigger the adaptive, reactive behaviors in the *Approach* step
- The duration of the update, if the duration last for a certain minutes (e.g. 3s), it will trigger the proactive or reactive behavior in the *Revision* step.


    stream_speech.py

## 4.2 Gaze recognition

Predict the customer intential level. The output can be used for several purposes: 

- Trigger the adaptive behavior in *approach* step


## 4.3 Dishes database

MongoDB, store the dishes. Organization look like:

- Category
	- name
	- taste
	- price
	- popularity
	- ingredient

Each part will have some descriptions so the robot can say something when justifications 


## 4.4 Dialog tree / dialog generation module

TBD