#!/usr/bin/python
#coding:utf-8
import qi
import sys
import time

class GazeAnalysis(object):
    def __init__(self, app):
        super(GazeAnalysis, self).__init__()
        app.start()
        session = app.session
        self.memory = session.service("ALMemory")

        self.subscriber1 = self.memory.subscriber("GazeAnalysis/PersonStartsLookingAtRobot")
        self.subscriber1.signal.connect(self.get_id)
        
        self.subscriber2 = self.memory.subscriber("GazeAnalysis/PersonStopsLookingAtRobot")
        self.subscriber2.signal.connect(self.delete_id)
        self.gaze = session.service("ALGazeAnalysis")
        self.gaze.subscribe("GazeAnalysis")
        self.got_gaze = False
        self.people_id = 0
    def get_id(self, id): #raise when detect the human gaze
        if id == []:
            self.people_id = 0
        else:
            self.people_id = id

    def delete_id(self, id): #raise when the cannot detect human gaze
        self.people_id = 0

    def on_gaze_tracked(self): #print the head_angle, gaze_direction and looking_robot_score, detail in http://doc.aldebaran.com/2-5/naoqi/peopleperception/algazeanalysis-api.html?highlight=gaze#PeoplePerception/Person/%3CID%3E/IsLookingAtRobot
        if self.people_id == 0:
            print "Can not find a people"
        else:
            head_key = 'PeoplePerception/Person/' + str(self.people_id) + '/HeadAngles'
            gaze_direction_key = 'PeoplePerception/Person/' + str(self.people_id) + '/GazeDirection'
            is_looking_at_robot_key = 'PeoplePerception/Person/' + str(self.people_id) + '/LookingAtRobotScore'
            head_angle = self.memory.getData(head_key)
            gaze_direction = self.memory.getData(gaze_direction_key)
            looking_robot_score = self.memory.getData(is_looking_at_robot_key)
            print "I have catched your attention!"
            print(head_angle)
            print(gaze_direction)
            print(looking_robot_score)
            
    def run(self):
        print "Start GazeAnalysis"
        try:
            while True:
                self.on_gaze_tracked()
                time.sleep(1)
        except KeyboardInterrupt:
            print "Interrupted by user, stopping GazeAnalysis"
            self.gaze.unsubscribe("GazeAnalysis")
            sys.exit(0)
'''
    def on_gaze_tracked(self, id):
        if id == []:
            self.got_gaze = False
            print "Lost people attention!"
        else:
        #elif not self.got_gaze:
            self.got_gaze = True
            head_key = 'PeoplePerception/Person/' + str(id) + '/HeadAngles'
            gaze_direction_key = 'PeoplePerception/Person/' + str(id) + '/GazeDirection'
            is_looking_at_robot_key = 'PeoplePerception/Person/' + str(id) + '/LookingAtRobotScore'
            head_angle = self.memory.getData(head_key)
            gaze_direction = self.memory.getData(gaze_direction_key)
            looking_robot_score = self.memory.getData(is_looking_at_robot_key)
            print "I have catched your attention!"
            
            #begin_time = time.time()
            print(head_angle)
            print(gaze_direction)
            print(looking_robot_score)

        #else:
            #if (time.time() - begin_time > 4):
                #print "I have catched your attention for 4s"
                #begin_time = time.time()
   '''             


'''
def main(session):
    gaze = session.service('ALGazeAnalysis')

    memory = session.service('ALMemory')
    subscriber = memory.subscriber("GazeDetected")
    subscriber.signal.connect(gazeanalysis)

    perception = session.service('ALPeoplePerception')
    mood = session.service('ALMood')
    while True:
        print("Human attention level: " + mood.attention())

        time.sleep(1)

'''

if __name__ == "__main__":
    session = qi.Session()
    PepperIP = "10.0.2.11"
    try:
        connection_url = "tcp://" + PepperIP + ":" + "9559"
        app = qi.Application(["GazeAnalysis", "--qi-url=" + connection_url])
        #session.connect("tcp://" + PepperIP + ":" + "9559")
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + PepperIP + "\" on port " + "9559" +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
    #main(session)
    gaze_analysis = GazeAnalysis(app)
    gaze_analysis.run()


