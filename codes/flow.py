#!/usr/bin/python
#coding:utf-8
#~ NAO_IP = "10.0.253.99" # Nao Alex Blue
#record from computer
from __future__ import division
from optparse import OptionParser
import naoqi
import numpy as np
import time
from naoqi import ALProxy, ALBroker, ALModule

NAO_IP = "10.0.2.12"

class TTSEventWatcher(ALModule):
	""" An ALModule to react to the ALTextToSpeech/Status event """

	def __init__(self, ip_robot, port_robot):
		super(TTSEventWatcher, self).__init__("tts_event_watcher")
		global memory
		global tts_event_watcher
		memory = ALProxy("ALMemory", ip_robot, port_robot)
		self.tts = ALProxy("ALAnimatedSpeech", ip_robot, port_robot)
		self.asr = ALProxy('ALSpeechRecognition',ip_robot,port_robot)
		self.mo = ALProxy('ALMotion',ip_robot,port_robot)
		self.stiffnesses  = 1.0
		#self.mo.setStiffnesses('Head',self.stiffnesses)
		self.mo.setStiffnesses('Body',self.stiffnesses)
		# self.aaw = ALProxy('ALBasicAwareness',ip_robot,port_robot)
		# self.aaw.stopAwareness()
		# self.aaw.setTrackingMode('Head')
		#self.asr.setLanguage("Chinese")
		# self.audio = ALProxy("ALAudioDevice",ip_robot,port_robot)
		#self.record = ALProxy("ALAudioRecorder",ip_robot,port_robot)
		# self.aup = ALProxy("ALAudioPlayer",ip_robot,port_robot)
		#self.record_path = '/home/nao/record.wav'


			# memory.raiseEvent("ALSpeechRecognition/Status","ListenOn")


	def on_tts_status(self, key, value, message):
		""" callback for event ALStatus """
		print "TTS Status value:", value


	def shutdown(self):
		self.shutdown_value = 1

#def greeting():
   # tts_event_watcher.tts.say("Hi, welcome to our restaurant")
   # tts_event_watcher.tts.say("You can read the menu on the table. I will leave you for a few minutes. You can call me whenever you want.")
    #code for leaving

#def approach():
    #code for approaching
#def elicit_preference():
    
#def recommend():

#def critique():


def main():
    parser = OptionParser()
    parser.add_option("--pip",
		help="Parent broker port. The IP address or your robot",
		dest="pip")
    parser.add_option("--pport",
		help="Parent broker port. The port NAOqi is listening to",
		dest="pport",
		type="int")
    parser.set_defaults(
		pip=NAO_IP,
		pport=9559)
    (opts, args_) = parser.parse_args()
    pip   = opts.pip
    pport = opts.pport
    # We need this broker to be able to construct
	# NAOqi modules and subscribe to other modules
	# The broker must stay alive until the program exists
    myBroker = naoqi.ALBroker("myBroker",
	   "0.0.0.0",   # listen to anyone
	   0,           # find a free port and use it
	   pip,         # parent broker IP
	   pport)       # parent broker port

    global tts_event_watcher
    tts_event_watcher = TTSEventWatcher(pip, pport)
    #tts_event_watcher.mo.mvoeInit()
	
    tts_event_watcher.mo.wakeUp()
    init_approach = tts_event_watcher.mo.moveTo(0.6,0,0)
    tts_event_watcher.mo.wait(init_approach,0)
    #tts_event_watcher.tts.say("Hi, welcome to our restaurant")

if __name__ == "__main__":
	main()