#!/usr/bin/python
#coding:utf-8
#~ NAO_IP = "10.0.253.99" # Nao Alex Blue
#record from computer

#if no route to host, run:   iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 1111 -j ACCEPT  

import qi
import sys
def main(session):
    
    # Note: Disable / enable some autonomousAbilities when we want to avoid unexpected robot behaviors
    # But we should enable some autonomousAbilities to keep the robot alive when we let the robot waiting
    # Detailed information can look into following link 
    #http://doc.aldebaran.com/2-5/ref/life/autonomous_abilities_management.html?highlight=autonomous
    life_service = session.service("ALAutonomousLife")
    life_service.setAutonomousAbilityEnabled("BasicAwareness", False)
    life_service.setAutonomousAbilityEnabled("AutonomousBlinking", True)
    
    # Move closer / away from the customer, but we have better make it more smart. 
    # A thought is to detect the location (x,y,z) an distance from the human, then go 30% closer to the human
    # E.g. float PeoplePerception/Person/<ID>/Distance
        #Distance (in meters) between the person and the camera.
    # Check this: http://doc.aldebaran.com/2-5/naoqi/peopleperception/alpeopleperception-api.html#PeoplePerception/Person/%3CID%3E/Distance
    # Also, waving detection might be possible, check this: http://doc.aldebaran.com/2-5/naoqi/peopleperception/alwavingdetection.html
    motion = session.service('ALMotion')
    motion.wakeUp()
    motion.moveInit()
    motion.moveTo(-0.3,0,0,_async=False)

    # More pre-built animations: 
    #http://doc.aldebaran.com/2-5/naoqi/motion/alanimationplayer-advanced.html#animationplayer-tags-pepper
    animation = session.service('ALAnimationPlayer')
    animation.run('animations/Stand/Gestures/BowShort_1') # Bow before serving the customers

    # This service allows the robot plays body animation and speaks synchronously.
    # Check it on http://doc.aldebaran.com/2-5/naoqi/audio/alanimatedspeech.html
    animated_speech = session.service("ALAnimatedSpeech")
    animated_speech.say("Hi ^start(animations/Stand/BodyTalk/BodyTalk_12), welcome to the HCI lab!")
    #configuration = {"bodyLanguageMode":"contextual"}
    #animated_speech.say("Hi, welcome to the HCI lab!", configuration)

    #tts = session.service("ALTextToSpeech")
    
    #animation.runTag('hello',_async=True)
    #animation.run('animations/Stand/Gestures/BowShort_1')
    #tts.say("Hi, welcome to our HCI lab")
    #animation.run('animations/Stand/Gestures/Enthusiastic_4')
    life_service.setAutonomousAbilityEnabled("BasicAwareness", True)

    """
    Body animations:
    1. 'animations/Stand/Gestures/BowShort_1'  --->   Greeting at the beginning
    2. 'animations/Stand/Gestures/Enthusiastic_4' , 'animations/Stand/Emotions/Positive/Peaceful_1'  ---> talking
    """

if __name__ == "__main__":
    session = qi.Session()
    PepperIP = "192.168.1.103"
    try:
        session.connect("tcp://" + PepperIP + ":" + "9559")
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + PepperIP + "\" on port " + "9559" +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
    main(session)
    